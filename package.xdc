/******************************************************************************
 * FILE PURPOSE: Package specification file
 ******************************************************************************
 * FILE NAME: package.xdc
 *
 * DESCRIPTION:
 *  This file contains the package specification for the vps Driver
 *
 * Copyright (C) 2016-2018, Texas Instruments, Inc.
 *****************************************************************************/
package ti.drv.vps[1, 0, 1, 13] {
    module Settings;
}

